package com.company.graph2;

import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        Algorithms dijkstra = new Algorithms();
        return dijkstra.goDijkstra(adjacencyMatrix, startIndex);
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        Algorithms prima = new Algorithms();
        return prima.goPrima(adjacencyMatrix);
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        Algorithms kruskal = new Algorithms();
        return kruskal.goKruskal(adjacencyMatrix);
    }
}
