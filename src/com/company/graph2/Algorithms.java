package com.company.graph2;

import java.util.*;

public class Algorithms {

	public String breadthFirst(boolean[][] adjacencyMatrix, int startIndex) {
		ArrayDeque<String> vertexDeque = new ArrayDeque<>();
		ArrayList<Vertex> vertexArrayList = new ArrayList<>();
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			vertexArrayList.add(new Vertex(i));
		}
		vertexArrayList.get(startIndex).setPassed();
		vertexDeque.offerLast("" + startIndex);
		String result = "";
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				if (adjacencyMatrix[startIndex][j] && !vertexArrayList.get(j).isPassed()) {
					vertexArrayList.get(j).setPassed();
					vertexDeque.offerLast("" + j);
				}
			}
			result += vertexDeque.peekFirst() + ",";
			startIndex = Integer.parseInt(vertexDeque.pollFirst());
		}
		return result.substring(0, result.length() - 1);
	}

	public boolean[][] redoBooleanMatrix(int[][] array) {
		boolean[][] booleans = new boolean[array.length][array.length];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				if (array[i][j] == 1) {
					booleans[i][j] = true;
				} else {
					booleans[i][j] = false;
				}
			}
		}
		return booleans;
	}

	HashMap<Integer, Integer> goDijkstra(int[][] adjacencyMatrix, int startIndex) {
		HashMap<Integer, ArrayList<Integer>> tempWeights = new HashMap<>();
		HashMap<Integer, Integer> resultMap = new HashMap<>();
		ArrayList<Vertex> vertexArrayList = new ArrayList<>();
		ArrayList<Integer> minWeights = new ArrayList<>();
		int minWeight;
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			tempWeights.put(i, new ArrayList<>());
			resultMap.put(i, 0);
			vertexArrayList.add(new Vertex(i));
		}
		vertexArrayList.get(startIndex).setPassed();
		for (int i = 0; i < adjacencyMatrix.length - 1; i++) {
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				if (adjacencyMatrix[startIndex][j] > 0 && !vertexArrayList.get(j).isPassed()) {//стандартный сценарий без приколов
					vertexArrayList.get(j).setRange(adjacencyMatrix[startIndex][j] + vertexArrayList.get(startIndex).getRange());
					tempWeights.get(j).add(vertexArrayList.get(j).getRange());
					minWeights.add(Collections.min(tempWeights.get(j)));
				}
				if (adjacencyMatrix[startIndex][j] == 0 && !vertexArrayList.get(j).isPassed() && tempWeights.get(j).size() != 0) {//случай с вершиной в которую не можем попасть, но до этого могли
					minWeights.add(Collections.min(tempWeights.get(j)));
				}
			}
			if (minWeights.isEmpty()) {//если вся строка нулевая
				return resultMap;
			}
			minWeight = Collections.min(minWeights);
			minWeights.clear();
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				if (tempWeights.get(j).contains(minWeight)) {
					vertexArrayList.get(j).setPassed();
					vertexArrayList.get(j).setRange(minWeight);
					startIndex = j;
					resultMap.put(j, minWeight);
					tempWeights.get(j).clear();
					break;
				}
			}
		}
		return resultMap;
	}

	public int goPrima(int[][] adjacencyMatrix) {
		ArrayList<Vertex> vertexArrayList = new ArrayList<>();
		HashMap<Integer, ArrayList<Integer>> weights = new HashMap<>();
		int startIndex = 0;
		int sumOfWeights = 0;
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			vertexArrayList.add(new Vertex(i));
			weights.put(vertexArrayList.get(i).getNumber(), new ArrayList<>());
		}
		vertexArrayList.get(startIndex).setPassed();
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				if (adjacencyMatrix[startIndex][j] > 0 && !vertexArrayList.get(j).isPassed()) {
					weights.get(j).add(adjacencyMatrix[startIndex][j]);
				}
			}
			ArrayList<Integer> minWeights = new ArrayList<>();
			for (int k = 0; k < adjacencyMatrix.length; k++) {
				if (!weights.get(k).isEmpty()) {
					minWeights.add(Collections.min(weights.get(k)));
				}
			}
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				if (!weights.get(j).isEmpty()) {
					if (Collections.min(weights.get(j)).equals(Collections.min(minWeights)) && !vertexArrayList.get(j).isPassed()) {
						vertexArrayList.get(j).setPassed();
						sumOfWeights += Collections.min(minWeights);
						weights.get(j).clear();
						startIndex = j;
						break;
					}
				}
			}
		}
		return sumOfWeights;
	}

	public int goKruskal(int[][] adjacencyMatrix) {
		int sumOfWeights = 0;
		HashMap<Integer, ArrayList<String>> weightsPairOfVertexesMap = new HashMap<>();
		ArrayList<HashSet<String>> setsOfVertexes = new ArrayList<>();
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			for (int j = i + 1; j < adjacencyMatrix.length; j++) {     //смотрим только верхний треугольник матрицы, т.к начинаем с диагонали, а она всегда 0, то начинаем с i+1
				if (adjacencyMatrix[i][j] > 0 && weightsPairOfVertexesMap.get(adjacencyMatrix[i][j]) == null) {
					weightsPairOfVertexesMap.put(adjacencyMatrix[i][j], new ArrayList<>());
				}
				if (adjacencyMatrix[i][j] > 0 && weightsPairOfVertexesMap.get(adjacencyMatrix[i][j]) != null) {
					weightsPairOfVertexesMap.get(adjacencyMatrix[i][j]).add(i + " " + j);
				}
			}
		}
		TreeMap<Integer, ArrayList<String>> weightsPairOfVertexesTreeMap = new TreeMap<>(weightsPairOfVertexesMap);
			/*
			 *создаем первое множество(а), так сказать базисное(ые)
			 */
			setsOfVertexes.add(new HashSet<>());
			String[] splitString1 = weightsPairOfVertexesTreeMap.firstEntry().getValue().get(0).split(" ");
			setsOfVertexes.get(0).add(splitString1[0]);
			setsOfVertexes.get(0).add(splitString1[1]);
			sumOfWeights += weightsPairOfVertexesTreeMap.firstKey();
		for (int i = weightsPairOfVertexesTreeMap.size(); i > 0; i--) {
			for (int j = 0; j < weightsPairOfVertexesTreeMap.firstEntry().getValue().size(); j++) {
				boolean flagForEmptiness = false; //если обе вершины не лежат в множестве
				boolean flagForNotEmptiness = false;//если обе вершины лежат
				ArrayList<Integer> foundSetsToMerge = new ArrayList<>();//множества где вершины встретились несколько раз
				String[] splitString = weightsPairOfVertexesTreeMap.firstEntry().getValue().get(j).split(" ");
				int iteration = -1;//итерации для получения индекса множества
				for (HashSet<String> vertexes : setsOfVertexes) {
					iteration++;
					if(vertexes.contains(splitString[0]) && vertexes.contains(splitString[1])){//если обе вершины лежат во множестве(плохой случай вес не прибавляем)
						flagForNotEmptiness = true;
					}
					if((!vertexes.contains(splitString[0]) || !vertexes.contains(splitString[1])) && !(!vertexes.contains(splitString[0]) && !vertexes.contains(splitString[1]))){//хотя бы одна вершина лежит, но не обе сразу не лежат
						vertexes.add(splitString[0]);
						vertexes.add(splitString[1]);
						flagForEmptiness = false;
						foundSetsToMerge.add(foundSetsToMerge.size(), iteration);//записываем индексы для слияния
					}
					if(!vertexes.contains(splitString[0]) && !vertexes.contains(splitString[1])){//обе вершины не лежат в множестве
						flagForEmptiness = true;
					}
				}
				if(foundSetsToMerge.size() > 1){//вершины встеритлись более 1 раза, слияние
					for (int k = 1; k < foundSetsToMerge.size(); k++) {
						TreeSet<String> strings = new TreeSet<>(setsOfVertexes.get(foundSetsToMerge.get(k)));
						int stringsSize = strings.size();
						for (int l = 0; l < stringsSize; l++) {
							setsOfVertexes.get(foundSetsToMerge.get(0)).add(strings.pollFirst());
						}
					}
					for (int k = 1; k < foundSetsToMerge.size(); k++) {
						setsOfVertexes.remove((int)foundSetsToMerge.get(k));
					}
				}
				if(flagForEmptiness){//создание нового множества
					setsOfVertexes.add(new HashSet<>());
					setsOfVertexes.get(setsOfVertexes.size() - 1).add(splitString[0]);
					setsOfVertexes.get(setsOfVertexes.size() - 1).add(splitString[1]);
				}
				foundSetsToMerge.clear();
				if(flagForNotEmptiness){//плохой случай, не добавляем вес
					continue;
				}
				//System.out.println(setsOfVertexes); //можно посмотреть как образовываются и склеиваются множества
				sumOfWeights += weightsPairOfVertexesTreeMap.firstKey();
			}
			weightsPairOfVertexesTreeMap.pollFirstEntry();
		}
		return sumOfWeights;
	}
}