package com.company.graph2;

public class Vertex {
	private boolean isPassed = false;
	private int number;
	private int range;

	public Vertex(int number, int range) {
		this.number = number;
		this.range = range;
	}

	public Vertex(int number) {
		this.number = number;
	}

	public boolean isPassed() {
		return isPassed;
	}

	public void setPassed() {
		isPassed = true;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getRange() {
		return range;
	}
}
